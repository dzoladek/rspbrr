﻿using GrovePi.Sensors;
using Raspberry.Configurations;
using Raspberry.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Raspberry.Sensors
{
    class MicSensor
    {
        private readonly ISoundSensor _sensor;
        private long _startTime;
        private long _endTime;
        private long _previousCheck;
        private bool _active;
        private readonly List<int> _sensorValues;

        public MicSensor(ISoundSensor micSensor)
        {
            _sensorValues = new List<int>();
            _sensor = micSensor;
        }

        public void RefreshState(int level)
        {
            var currentTimestamp = Variables.CurrentTimestamp;
            var sensorvalue = _sensor.SensorValue();

            if (currentTimestamp > _previousCheck)
            {
                _previousCheck = currentTimestamp;
                AddSensorValue(sensorvalue);

                double avreageValue = _sensorValues.Average();
                System.Diagnostics.Debug.WriteLine("Sound is " + avreageValue);
                if (avreageValue > level)
                {
                    
                    if (!_active)
                    {
                        _startTime = currentTimestamp;
                        _active = true;
                    }
                    _endTime = currentTimestamp;
                }
                else
                    _active = false;
            }
        }

        public void AddSensorValue(int value)
        {
            _sensorValues.Add(value);
            if(_sensorValues.Count > AlarmConfiguration.Microphone.AvreageSamples)
                _sensorValues.RemoveAt(0);
        }

        public bool DetectScream(int duration)
        {
            if (_sensorValues.Count == AlarmConfiguration.Microphone.AvreageSamples && _endTime - _startTime > duration && _active)
            {
                _sensorValues.Clear();
                return true;
            }
            return false;
        }

        public void AlarmIsDisabled()
        {
            _sensorValues.Clear();
        }
    }
}
