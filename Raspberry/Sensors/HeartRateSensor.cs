﻿using GrovePi.Sensors;
using Raspberry.Helpers;
using System.Collections.Generic;

namespace Raspberry.Sensors
{
    class HeartRateSensor
    {
        private readonly IRotaryAngleSensor _sensor;

        private int _currentState;
        private long _lastCheck;
        private HearthBeatData _beat = new HearthBeatData(0);

        private readonly List<HearthBeatData> _beats = new List<HearthBeatData>();

        public HeartRateSensor(IRotaryAngleSensor heartRateSensor)
        {
            _sensor = heartRateSensor;
        }
        public void RefreshState(int duration)
        {
            var currentTimestamp = Variables.CurrentTimestamp;
            if (_lastCheck < currentTimestamp)
            {
                int sensorValue = (_sensor.SensorValue() == 0) ? 0 : 1;

                RemoveOldBeats(currentTimestamp, duration);

                if (sensorValue == 1 && _currentState == 0)
                {
                    _beat.End = currentTimestamp;
                    if(_beat.IsValid())
                        AddBeat(_beat);
                    _beat = new HearthBeatData(currentTimestamp);
                }
                else if(sensorValue == 0 && _currentState == 1)
                {
                    _beat.Mid = currentTimestamp;
                }

                _currentState = sensorValue;
                _lastCheck = currentTimestamp;
            }
        }
        public int HearthBeat()
        {
            return _currentState;
        }

        public void AddBeat(HearthBeatData beat)
        {
            _beats.Add(beat);
        }
        public void RemoveOldBeats(long currentTimestamp, int duration)
        {
            for(var i = _beats.Count -1; i >=0 ; i--)
            {
                if (currentTimestamp - _beats[i].Start > duration)
                    _beats.RemoveAt(i);
            }
        }
        public int HearthRate()
        {
            double diff = 0;
            int hearthRate;
            for(var i = 1; i< _beats.Count; i++)
                diff += (int)(_beats[i].Duration());

            diff /= (1000 *60) * (_beats.Count -1);
            hearthRate = (diff != 0) ? (int)(1/diff ) : -1;
            //System.Diagnostics.Debug.WriteLine("Hearthrate is " + _currentHearthRate.ToString());
            return hearthRate;
        }

    }
}
