﻿using GrovePi.I2CDevices;
using Raspberry.Enums;

namespace Raspberry.Sensors
{
    internal class Display
    {
        private string _currentText = "";
        private readonly IRgbLcdDisplay _sensor;

        public Display(IRgbLcdDisplay display)
        {
            _sensor = display;
        }

        public void SetText(string text)
        {
            if (_currentText != text)
            {
                _currentText = text;
                _sensor.SetText(text);
            }
               
        }

        public void SetColor(Colors color)
        {
            switch (color)
            {
                case Colors.Red : _sensor.SetBacklightRgb(255, 0, 0); break;
                case Colors.Green: _sensor.SetBacklightRgb(0, 255, 0); break;
                case Colors.Blue: _sensor.SetBacklightRgb(0, 0, 255); break;
                case Colors.Yellow: _sensor.SetBacklightRgb(255, 255, 0); break;
                default: _sensor.SetBacklightRgb(20, 20, 20); break;
            }
        }
    }
}
