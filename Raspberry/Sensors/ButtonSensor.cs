﻿using GrovePi.Sensors;
using Raspberry.Enums;
using Raspberry.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Raspberry.Sensors
{
    internal class ButtonSensor
    {
        private readonly IButtonSensor _sensor;
        private SensorStatus _currentStatus = SensorStatus.Off;
        private long _lastCheck;
        private readonly List<Click> _pattern = new List<Click>();
        private readonly List<ClickInfo> _clicksInfos = new List<ClickInfo>();

        public ButtonSensor(IButtonSensor button)
        {
            _sensor = button;
        }

        public void RefreshState()
        {
            var currentTimestamp = Variables.CurrentTimestamp;
            if (_lastCheck < currentTimestamp)
            {
                var state = _sensor.CurrentState;
                _lastCheck = currentTimestamp;
                if (_currentStatus != state)
                {
                    if (state == SensorStatus.On)
                    {
                        if (_pattern.Count != 0)
                            RemoveCurrentPattern();
                        _clicksInfos.Add(new ClickInfo(currentTimestamp));
                    }
                    else
                    {
                        _clicksInfos[_clicksInfos.Count - 1].End = currentTimestamp;
                    }
                }
                else if (_clicksInfos.Count > 0 && currentTimestamp - _clicksInfos[_clicksInfos.Count - 1].End > 800 && _currentStatus == SensorStatus.Off)
                {
                    FilterClickInfos();
                    CalculatePattern();
                    PrintPattern();
                }

                _currentStatus = state;
            }
        }

        private void CalculatePattern()
        {
            foreach(var clickInfo in _clicksInfos)
                _pattern.Add((Click)clickInfo);
            _clicksInfos.Clear();
        }

        private void FilterClickInfos()
        {
            for (int i = _clicksInfos.Count - 1; i > 0; i--)
            {
                if (_clicksInfos[i].Start - _clicksInfos[i - 1].End < (int)Click.Short)
                {
                    _clicksInfos[i - 1].End = _clicksInfos[i].End;
                    _clicksInfos.RemoveAt(i);
                }
            }
        }

        public void PrintPattern()
        {
            string patt = "";
            for (int i = 0; i < _pattern.Count; i++)
                patt += Enum.GetName(typeof(Click), _pattern[i]);
            System.Diagnostics.Debug.WriteLine(patt);
        }

        public bool CheckPattern( List<Click> toCheck)
        {
            var state = toCheck.SequenceEqual(_pattern);
            if (state)
                RemoveCurrentPattern();
            return state;
        }

        public bool CheckPattern(Click[] toCheck)
        {
            var state = toCheck.SequenceEqual(_pattern);
            if (state)
                RemoveCurrentPattern();
            return state;
        }

        public SensorStatus GetStatus()
        {
            return _sensor.CurrentState;
        }

        public void  RemoveCurrentPattern()
        {
            _clicksInfos.Clear();
            _pattern.Clear();
        }
    }
}
