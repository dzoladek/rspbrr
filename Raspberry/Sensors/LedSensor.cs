﻿using GrovePi.Sensors;
using Raspberry.Helpers;

namespace Raspberry.Sensors
{
    class LedSensor
    {
        private ILed _sensor;
        private long _nextTimestamp = 0;
        private SensorStatus _currentStatus = SensorStatus.Off;
        private bool _active = false;
        private int _effectSpeed = 500;

        public LedSensor(ILed led)
        {
           _sensor = led;
        }

        public void RefreshState()
        {
            var currentTimestamp = Variables.CurrentTimestamp;
            if (currentTimestamp > _nextTimestamp)
            {
                if (_currentStatus == SensorStatus.On)
                {
                    _currentStatus = SensorStatus.Off;
                    _sensor.ChangeState(_currentStatus);
                    _nextTimestamp = currentTimestamp + _effectSpeed;
                }
                else if (_active)
                {
                    _currentStatus = SensorStatus.On;
                    _sensor.ChangeState(_currentStatus);
                   _nextTimestamp = currentTimestamp + _effectSpeed;
                }
           }
        }
        public void Disable()
        {
            _active = false;
            _sensor.ChangeState(SensorStatus.Off);
        }
        public void Enable()
        {
            _active = true;
        }
        public void Enable(int effectSpeed)
        {
            this._effectSpeed = effectSpeed;
            _active = false;
        }
        public void SetState(SensorStatus status)
        {
            _sensor.ChangeState (status);
        }

    }
}
