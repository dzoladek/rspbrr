﻿using GrovePi.Sensors;
using Raspberry.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.Sensors
{
    internal class BuzzerSensor
    {
        private readonly IBuzzer _sensor;
        private long _nextTimestamp;
        private SensorStatus _currentStatus = SensorStatus.Off;
        private bool _active;
        private int _effectSpeed = 500;

        public BuzzerSensor(IBuzzer buzzer)
        {
            _sensor = buzzer;
        }

        public void RefreshState()
        {
            var currentTimestamp = Variables.CurrentTimestamp;
            if (currentTimestamp > _nextTimestamp)
            {
                if (_currentStatus == SensorStatus.On)
                {
                    _currentStatus = SensorStatus.Off;
                    _sensor.ChangeState(_currentStatus);
                    _nextTimestamp = currentTimestamp + _effectSpeed;
                }
                else if (_active)
                {
                    _currentStatus = SensorStatus.On;
                    _sensor.ChangeState(_currentStatus);
                    _nextTimestamp = currentTimestamp + _effectSpeed;
                }
            }
        }

        public void Disable()
        {
            _active = false;
            _sensor.ChangeState(SensorStatus.Off);
        }

        public void Enable()
        {
            _active = true;
        }

        public void Enable(int effectSpeed)
        {
            this._effectSpeed = effectSpeed;
            _active = false;
        }

        public void SetState(SensorStatus status)
        {
            _sensor.ChangeState(status);
        }
    }
}
