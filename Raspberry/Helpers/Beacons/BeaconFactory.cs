﻿namespace W10IOT.Helpers.Beacons
{
    class BeaconFactory
    {
        public byte Major { get; set; }
        public byte Minor { get; set; }
    }
}
