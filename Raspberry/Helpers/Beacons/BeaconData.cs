﻿using Raspberry.Peripherals;
using System.Collections.Generic;
using System.Linq;

namespace Raspberry.Helpers.Beacons
{
    class BeaconData
    {
        private string _address;
        private List<int> _signals = new List<int>();

        public BeaconData(string adr)
        {
            _address = adr;
        }
        public string GetAddress()
        {
            return _address;
        }
        public void AddSignal(int signal)
        {           
            _signals.Add(signal);
            if (_signals.Count >= Bluetooth.BluetoothSignalMaxChecks)
                _signals.RemoveAt(0);
        }
        public double AvreageSignal()
        {
            return _signals.Average();
        }
    }
}
