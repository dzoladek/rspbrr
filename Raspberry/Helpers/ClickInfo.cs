﻿using System;
using Raspberry.Enums;
using static Raspberry.Sensors.ButtonSensor;

namespace Raspberry.Helpers
{
    class ClickInfo
    {
        public long Start { get; set; }
        public long End { get; set; }

        public ClickInfo(long start)
        {
            Start = start;
        }

        public static explicit operator Click(ClickInfo clickInfo)
        {
            long clicklength = clickInfo.End - clickInfo.Start;
            Click click = Click.Short;
            
            foreach(int c in Enum.GetValues(typeof(Click)))
            {
                if (clicklength > c)
                {
                    click = (Click)c;
                }
            }
            return click;
        }
    }
}
