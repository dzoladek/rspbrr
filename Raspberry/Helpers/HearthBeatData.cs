﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.Helpers
{
    class HearthBeatData
    {
        public long Start { get; set; }
        public long Mid { get; set; }
        public long End { get; set; }

        public HearthBeatData(long start)
        {
            Start = start;
        }


        public bool IsValid()
        {
            if (Mid - Start > (End - Start) * 0.3)
                return true;
            return false;
        }

        public long Duration()
        {
            return End - Start;
        }
    }
}
