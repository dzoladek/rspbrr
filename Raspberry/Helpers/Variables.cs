﻿using System;

namespace Raspberry.Helpers
{
    class Variables
    {
        public static long CurrentTimestamp => DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    }

}
