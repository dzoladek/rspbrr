﻿using Windows.ApplicationModel.Background;
using GrovePi;
using Raspberry.Sensors;
using static Raspberry.Sensors.ButtonSensor;
using Raspberry.Peripherals;
using Raspberry.Managers;
using Microsoft.WindowsAzure.MobileServices;
using Raspberry.DataModel;
using Raspberry.Services;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace Raspberry
{
    public sealed class StartupTask : IBackgroundTask
    {
        
        //private static readonly MobileServiceClient _mobileService = new MobileServiceClient("https://homecarebackend.azurewebsites.net");
        


        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var alarmManager = new AlarmManager();
            DeviceRegistration.Configure();

            //IMobileServiceTable<Alert> alertTable = _mobileService.GetTable<Alert>();

            while (true)
            {
                 if (!DeviceState.StandBy && DeviceRegistration.ValidData)
                 {
                    SensorsManager.RefreshState();
                    alarmManager.RefreshState();

                    SensorsManager.Display.SetText("Your heartrate : " + SensorsManager.HeartRateSensor.HearthRate());
                    System.Diagnostics.Debug.WriteLine("Hearthrate is " + SensorsManager.HeartRateSensor.HearthBeat());
                }
                else
                    DeviceRegistration.RefreshState();

                if (!DeviceRegistration.RegistrationActive)
                {
                    DeviceState.RefreshState(
                        () => alarmManager = new AlarmManager(),
                        null
                    );
                }
            }
        }
    }
}
