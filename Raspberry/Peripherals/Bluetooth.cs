﻿using System.Collections.Generic;
using System.Linq;
using Windows.Devices.Bluetooth.Advertisement;
using W10IOT.Helpers.Beacons;
using UniversalBeaconLibrary.Beacon;
using Raspberry.Helpers.Beacons;

namespace Raspberry.Peripherals
{
    class Bluetooth
    {
        public static int BluetoothSignalMaxChecks => 1000;
        private long _lastCheck;
        private readonly List<BeaconData> _lista;
        //config
        private readonly List<BeaconFactory> _beacons = new List<BeaconFactory>();

        private readonly BluetoothLEAdvertisementWatcher _watcher;
        private readonly BeaconManager _beaconManager;

        public Bluetooth()
        {
            _beacons.Add(new BeaconFactory() { Major = 1, Minor = 2 });
            _beacons.Add(new BeaconFactory() { Major = 2, Minor = 1 });

            _lista = new List<BeaconData>();

            _beaconManager = new BeaconManager();
            _watcher = new BluetoothLEAdvertisementWatcher { ScanningMode = BluetoothLEScanningMode.Active };
            _watcher.Received += WatcherOnReceived;
            _watcher.Start();
        }

        private void WatcherOnReceived(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs eventArgs)
        {
            _beaconManager.ReceivedAdvertisement(eventArgs);
        }
        public void RefreshState()
        {
            long currentTimestamp = Helpers.Variables.CurrentTimestamp;
            if (_lastCheck < currentTimestamp)
            {
                _lastCheck = currentTimestamp;
                foreach (var bluetoothBeacon in _beaconManager.BluetoothBeacons.ToList())
                {
                    if (bluetoothBeacon.BeaconType == Beacon.BeaconTypeEnum.iBeacon)
                    {
                        if (ValidCredentials(bluetoothBeacon))
                        {
                            AddBeaconData(bluetoothBeacon);
                        }
                    }
                }
            }
        }

        public double GetCurrentSignal()
        {
            return GetFarrest().AvreageSignal();
        }

        public void AddBeaconData(Beacon bluetoothBeacon)
        {
            BeaconData currentBeaconData = _lista.SingleOrDefault(x => x.GetAddress() == bluetoothBeacon.BluetoothAddressAsString);

            if (currentBeaconData == null)
            {
                currentBeaconData = new BeaconData(bluetoothBeacon.BluetoothAddressAsString);
                _lista.Add(currentBeaconData);
            }
            currentBeaconData.AddSignal(bluetoothBeacon.Rssi);
        }
        public BeaconData GetFarrest()
        {
            double lowestSignal = 0;
            BeaconData _farrest = null;
            foreach( BeaconData beaconData in _lista)
            {
                double signal = beaconData.AvreageSignal();
                if(signal < lowestSignal)
                {
                    lowestSignal = signal;
                    _farrest = beaconData;
                }
            }
            return _farrest;
        }
        public double AvreageSignal()
        {
            List<double> signals = new List<double>();
            foreach (BeaconData beaconData in _lista)
            {
                signals.Add(beaconData.AvreageSignal());
            }
            return (signals.Count == 0) ? 1 : signals.Average();
        }
        public List<double> AllAvreageSignals()
        {
            List<double> signals = new List<double>();
            foreach (BeaconData beaconData in _lista)
            {
                signals.Add(beaconData.AvreageSignal());
            }
            return signals;
        }

        private bool ValidCredentials(Beacon bluetoothBeacon)
        {
            bool isValid = false;
            foreach (BeaconFactory beacon in _beacons)
            {
                if (GetMinor(bluetoothBeacon) == beacon.Minor && GetMajor(bluetoothBeacon) == beacon.Major)
                {
                    isValid = true;
                    break;
                }
            }
            return isValid;
        }
        private byte GetMajor(Beacon bluetoothBeacon) => bluetoothBeacon.BeaconFrames[0].Payload[19];
        private byte GetMinor(Beacon bluetoothBeacon) => bluetoothBeacon.BeaconFrames[0].Payload[21];
    }
}
