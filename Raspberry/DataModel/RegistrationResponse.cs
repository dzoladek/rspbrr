﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.DataModel
{
    class RegistrationResponse
    {
        [JsonProperty("sessionid")]
        public string SessionId { get; set; }
        [JsonProperty("pin")]
        public string Pin { get; set; }
    }
}
