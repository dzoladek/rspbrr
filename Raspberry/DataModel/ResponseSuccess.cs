﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.DataModel
{
    class ResponseSuccess
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}
