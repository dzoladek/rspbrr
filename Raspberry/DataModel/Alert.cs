﻿using System;
using Newtonsoft.Json;

namespace Raspberry.DataModel
{
    public sealed class Alert
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("clientid")]
        public string SessionId { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("alertType")]
        public AlarmType AlarmType{ get; set; }
    }
}
