﻿using GrovePi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry
{
    static class GpioConfig
    {
        public static Pin LedPin => Pin.DigitalPin4;
        public static Pin BuzzerPin => Pin.DigitalPin2;
        public static Pin ButtonPin => Pin.DigitalPin3;
        public static Pin MicPin => Pin.AnalogPin0;
        public static Pin HeartratePin => Pin.AnalogPin1;
        
    }
}
