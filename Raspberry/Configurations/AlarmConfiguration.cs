﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.Configurations
{
    class AlarmConfiguration
    {
        public static class Bluetooth{
            public static int MinimalSignalLevel => -75;
            public static long OutOfRangeTime => 5000;

            public static int MinimalSignalLevelDiff => 2;
            public static long TimeForStrengthChecking => 20000;
        }

        public static class HearthRate
        {
            public static int Limit => 120;
            public static int AvreageTimeChecking => 10000;
        }

        public static class Microphone
        {
            public static int ScreamLevel => 170;
            public static int ScreamLevelAlarmTime => 1000;
            public static int AvreageSamples => 30;
        }
       
    }
}
