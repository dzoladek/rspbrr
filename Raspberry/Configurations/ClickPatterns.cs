﻿using Raspberry.Enums;
using static Raspberry.Sensors.ButtonSensor;

namespace Raspberry
{
    static class ClickPatternsConfiguration
    {
        public static Click[] DisableAlarmPattern => new Click[] { Click.Long, Click.Long, Click.Long };
        public static Click[] StandBy => new Click[] { Click.SuperLong };
        public static Click[] Registration => new Click[] { Click.Short, Click.Short};
        public static Click[] Ok => new Click[] { Click.Long };
    }
}
