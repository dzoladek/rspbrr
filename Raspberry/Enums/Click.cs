﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.Enums
{
    public enum Click { Short = 50, Long = 600, SuperLong = 2500 };
}
