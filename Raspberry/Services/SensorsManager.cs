﻿using GrovePi;
using Raspberry.Configurations;
using Raspberry.Peripherals;
using Raspberry.Sensors;

namespace Raspberry.Managers
{
    class SensorsManager
    {
        public static readonly LedSensor Led;
        public static readonly BuzzerSensor Buzzer;
        public static readonly MicSensor MicSensor;
        public static readonly ButtonSensor ButtonSensor;
        public static readonly HeartRateSensor HeartRateSensor;
        public static readonly Display Display;
        public static readonly Bluetooth Bluetooth;

        static SensorsManager()
        {
            IBuildGroveDevices deviceFactory = DeviceFactory.Build;

            Led = new LedSensor(deviceFactory.Led(GpioConfig.LedPin));
            Buzzer = new BuzzerSensor(deviceFactory.Buzzer(GpioConfig.BuzzerPin));
            MicSensor = new MicSensor(deviceFactory.SoundSensor(GpioConfig.MicPin));
            ButtonSensor = new ButtonSensor(deviceFactory.ButtonSensor(GpioConfig.ButtonPin));
            HeartRateSensor = new HeartRateSensor(deviceFactory.RotaryAngleSensor(GpioConfig.HeartratePin));
            Display = new Display(deviceFactory.RgbLcdDisplay());
            Bluetooth = new Bluetooth();
        }


        public static void RefreshState() {
            Led.RefreshState();
            Bluetooth.RefreshState();
            Buzzer.RefreshState();
            HeartRateSensor.RefreshState(AlarmConfiguration.HearthRate.AvreageTimeChecking);
            MicSensor.RefreshState(AlarmConfiguration.Microphone.ScreamLevel);
        }


    }
}
