﻿using Raspberry.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Raspberry.DataModel;
using Raspberry.Configurations;
using Raspberry.Services;

namespace Raspberry.Managers
{
    internal class AlarmManager
    {

        public bool AlarmEnabled { get; private set; }
        private AlarmType _enabledType;

        private long BluetoothOutOfRangeStart;
        private bool BluetoothOutOfRangeEnabled;

        private List<double> BluetoothMovingSignal = new List<double>();
        private long BluetoothMovingStart;
        private bool BluetoothMovingEnabled;


        public void RefreshState()
        {
            CheckOutOfRange();
            CheckScreaming();
            CheckIfMoves();
            CheckDisablePattern();
        }

        public void CheckIfMoves()
        {
            var currentAvgSignal = SensorsManager.Bluetooth.AllAvreageSignals();
            var validSignals = currentAvgSignal.Count == BluetoothMovingSignal.Count;
            if (validSignals)
            {
                if (currentAvgSignal.Where((t, i) => Math.Abs(t - BluetoothMovingSignal[i]) > AlarmConfiguration.Bluetooth.MinimalSignalLevelDiff).Any())
                {
                    validSignals = false;
                }
            }
            if (validSignals && currentAvgSignal.Count > 0)
            {
                if (!BluetoothMovingEnabled)
                {
                    BluetoothMovingEnabled = true;
                    BluetoothMovingSignal = currentAvgSignal;
                    BluetoothMovingStart = Variables.CurrentTimestamp;
                }
                else if (!AlarmEnabled && Variables.CurrentTimestamp - BluetoothMovingStart > AlarmConfiguration.Bluetooth.TimeForStrengthChecking)
                {
                    EnableAlarm(AlarmType.NotMoving);
                    BluetoothMovingEnabled = false;
                }
            }
            else
            {
                BluetoothMovingEnabled = false;
                BluetoothMovingSignal = currentAvgSignal;
                DisableAlarm(AlarmType.NotMoving);
            }
        }

        public void CheckOutOfRange()
        {
            var farrest = SensorsManager.Bluetooth.GetFarrest();
            if (farrest != null && farrest.AvreageSignal() < AlarmConfiguration.Bluetooth.MinimalSignalLevel)
            {
                if (!BluetoothOutOfRangeEnabled)
                {
                    BluetoothOutOfRangeEnabled = true;
                    BluetoothOutOfRangeStart = Variables.CurrentTimestamp;
                }
                else if (!AlarmEnabled && Variables.CurrentTimestamp - BluetoothOutOfRangeStart > AlarmConfiguration.Bluetooth.OutOfRangeTime)
                {
                    BluetoothOutOfRangeEnabled = false;
                    EnableAlarm(AlarmType.OutOfRange);
                }
            }
            else
                DisableAlarm(AlarmType.OutOfRange);
        }

        public void CheckScreaming()
        {
            if (SensorsManager.MicSensor.DetectScream(AlarmConfiguration.Microphone.ScreamLevelAlarmTime))
                EnableAlarm(AlarmType.Screaming);
        }

        public void CheckHeartRate()
        {
            if (SensorsManager.HeartRateSensor.HearthRate() > AlarmConfiguration.HearthRate.Limit)
                EnableAlarm(AlarmType.HeartRate);
        }

        public void EnableAlarm(AlarmType type)
        {
            if (!AlarmEnabled)
            {
                AlarmEnabled = true;
                _enabledType = type;
                SensorsManager.Buzzer.Enable();
                SensorsManager.Led.Enable();
                Alert alert = new Alert
                {
                    Id = Guid.NewGuid().ToString(),
                    SessionId = DeviceRegistration.SessionID,
                    AlarmType = type,
                    Description = "Cosiek janusz"
                };
                SendAlarm(alert);
            }
        }

        public void DisableAlarm()
        {
            if (AlarmEnabled)
            {
                SensorsManager.Buzzer.Disable();
                SensorsManager.Led.Disable();
                SensorsManager.MicSensor.AlarmIsDisabled();
                AlarmEnabled = false;
            }
        }

        public void DisableAlarm(AlarmType type)
        {
            if (type == _enabledType)
                DisableAlarm();
        }

        public void CheckDisablePattern()
        {
            if (SensorsManager.ButtonSensor.CheckPattern(ClickPatternsConfiguration.DisableAlarmPattern))
                DisableAlarm();
        }

        public async void SendAlarm(Alert alert) => await ApiClient.SendAlert(alert);
    }
}
