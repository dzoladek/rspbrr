﻿using System;

namespace Raspberry.Managers
{
    static class DeviceState
    {

        static bool _standBy;
        public static bool StandBy => _standBy;

        static DeviceState()
        {
            Disabled();
        }

        public static void RefreshState(Action enabledDelegate, Action disabledDelegate)
        {
            if (SensorsManager.ButtonSensor.CheckPattern(ClickPatternsConfiguration.StandBy))
            {
                if (StandBy)
                {
                    Enabled();
                    enabledDelegate();
                }
                else
                {
                    Disabled();
                    disabledDelegate();
                }
            }
        }

        public static void Disabled()
        {
            SensorsManager.Display.SetColor(Enums.Colors.Red);
            SensorsManager.Display.SetText("Device turned off");
            SensorsManager.Led.Disable();
            SensorsManager.Buzzer.Disable();
            _standBy = false;
        }

        public static void Enabled()
        {
            SensorsManager.Display.SetColor(Enums.Colors.Red);
            SensorsManager.Display.SetText("Device turned on");
            SensorsManager.Led.Disable();
            SensorsManager.Buzzer.Disable();
            _standBy = true;
        }

    }
}
