﻿using Newtonsoft.Json;
using Raspberry.DataModel;
using Raspberry.Managers;

namespace Raspberry.Services
{
    class DeviceRegistration
    {
        private static string _sessionId;
        private static string _deviceId;

        public static string DeviceID => _deviceId;
        public static string SessionID => _sessionId;

        private static bool _dataLoaded;

        private static bool _validData;
        public static bool ValidData => _validData;

        private enum RegistrationStatus {None, Started, Error, Success };
        private static RegistrationStatus _registrationStatus = RegistrationStatus.None;
        public static bool RegistrationActive => (_registrationStatus != RegistrationStatus.None);

        private static void LoadSavedData()
        {
            if (!_dataLoaded)
            {
                _deviceId = "5";
                _sessionId = "6";
                _dataLoaded = false;
            }
        }

        public static async void Configure()
        {
            LoadSavedData();
            var responseStatus = JsonConvert.DeserializeObject<ResponseSuccess>(await ApiClient.CheckPairing());
            if (responseStatus.Success)
                _validData = true;
        }

        public static void RefreshState()
        {
            if (_registrationStatus == RegistrationStatus.None)
            {
                if (SensorsManager.ButtonSensor.CheckPattern(ClickPatternsConfiguration.Registration) || !_validData)
                {
                    DeviceState.Disabled();
                    Register();
                }
            }
            else if(_registrationStatus == RegistrationStatus.Error || _registrationStatus == RegistrationStatus.Success)
            {
                if(SensorsManager.ButtonSensor.CheckPattern(ClickPatternsConfiguration.Ok)){
                    _registrationStatus = RegistrationStatus.None;
                    DeviceState.Disabled();
                }
            }
            
        }
        public static async void Register()
        {
            _registrationStatus = RegistrationStatus.Started;
            SensorsManager.Display.SetColor(Enums.Colors.Green);
            SensorsManager.Display.SetText("Waiting for server response");
            var registrationResponse = JsonConvert.DeserializeObject<RegistrationResponse>(await ApiClient.RegisterDevice());
            SensorsManager.Display.SetText("Your pin " + registrationResponse.Pin);
            ResponseSuccess responseSuccess = null;
            for (int counter = 0; counter <4; counter++)
            {
                responseSuccess = JsonConvert.DeserializeObject<ResponseSuccess>(await ApiClient.CheckPairing(registrationResponse.SessionId));
                if (responseSuccess.Success)
                {
                    _sessionId = registrationResponse.SessionId;
                    SensorsManager.Display.SetColor(Enums.Colors.Green);
                    SensorsManager.Display.SetText("Success !!");
                    _registrationStatus = RegistrationStatus.Success;
                    _validData = true;
                    return;
                }
            }
            if (!responseSuccess.Success)
            {
                _registrationStatus = RegistrationStatus.Error;
                SensorsManager.Display.SetColor(Enums.Colors.Green);
                SensorsManager.Display.SetText("Failed !!");
            }
        }
    }
}
