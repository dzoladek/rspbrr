﻿using Newtonsoft.Json;
using Raspberry.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Raspberry.Services;

namespace Raspberry.Managers
{
    static class ApiClient
    {
        private const string API_URL = "http://homecarewebservice.azurewebsites.net/api/";
        

        public static async Task<string> SendAlert(Alert alert) => await Request("Alerts", alert);
        public static async Task<string> RegisterDevice() => await Request("Register", new { DeviceID = DeviceRegistration.DeviceID });
        public static async Task<string> CheckPairing(string sessionID) => await Request("RegisterStatus", new { SessionID = sessionID });
        public static async Task<string> CheckPairing() => await Request("RegisterStatus", new { SessionID = DeviceRegistration.SessionID});


        public static async Task<string> Request(string ApiEndpoint,object data)
        {

            try
            {
                var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(data));
                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                var httpClient = new HttpClient();
                var httpResponse =
                    await httpClient.PostAsync("http://homecare.azurewebsites.net/api/alerts/" + ApiEndpoint,
                        httpContent);
                if (httpResponse.Content != null)
                {
                    var responseContent = await httpResponse.Content.ReadAsStringAsync();
                    return responseContent;
                }
            }
            catch (Exception)
            {
                
            }
            return "";
        }
    }
}
